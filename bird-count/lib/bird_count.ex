defmodule BirdCount do
  def today([]), do: nil
  def today([today | _]), do: today

  def increment_day_count([]), do: [1]
  def increment_day_count([today | others]), do: [today + 1 | others]

  def has_day_without_birds?(list), do: 0 in list

  def total([]), do: 0
  def total([head | tail]), do: head + total(tail)

  def busy_days(list) do
    find_busy_days(list, 0)
  end

  defp find_busy_days([], big_days), do: big_days
  defp find_busy_days([head | tail], big_days) when head > 4, do: find_busy_days(tail, big_days + 1)
  defp find_busy_days([_head | tail], big_days), do: find_busy_days(tail, big_days)
end
