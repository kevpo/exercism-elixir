defmodule LibraryFees do
  @seconds_in_day 24 * 60 * 60

  def datetime_from_string(string) do
    NaiveDateTime.from_iso8601!(string)
  end

  def before_noon?(datetime) do
   time = NaiveDateTime.to_time(datetime)
   noon = ~T[12:00:00]

   case Time.compare(time, noon) do
    :lt -> true
    _ -> false
   end
  end

  def return_date(checkout_datetime) do
    checkout_datetime
    |> before_noon?()
    |> get_seconds_to_add()
    |> add_seconds_to_date(checkout_datetime)
    |> NaiveDateTime.to_date()
  end

  defp get_seconds_to_add(true), do: 28 * @seconds_in_day
  defp get_seconds_to_add(false), do: 29 * @seconds_in_day

  defp add_seconds_to_date(seconds, checkout_datetime) do
    NaiveDateTime.add(checkout_datetime, seconds)
  end

  def days_late(planned_return_date, actual_return_datetime) do
    actual_return_date = NaiveDateTime.to_date(actual_return_datetime)
    diff = Date.diff(actual_return_date, planned_return_date)

    max(diff, 0)
  end

  def monday?(datetime) do
    monday = 1

    day = datetime
    |> NaiveDateTime.to_date()
    |> Date.day_of_week()

    monday == day
  end

  def calculate_late_fee(checkout, return, rate) do
    checkout_date_time = datetime_from_string(checkout)
    requried_return_date = return_date(checkout_date_time)
    actual_return_date_time = datetime_from_string(return)

    late_days = days_late(requried_return_date, actual_return_date_time)
    late_fee = late_days * rate

    actual_return_date_time
    |> monday?()
    |> apply_discount(late_fee)
  end

  defp apply_discount(true, late_fee), do: half_off(late_fee)
  defp apply_discount(false, late_fee), do: late_fee

  defp half_off(late_fee) do
    floor(late_fee - 0.5 * late_fee)
  end
end
