defmodule RemoteControlCar do
  @enforce_keys [:nickname]
  defstruct [:nickname, battery_percentage: 100, distance_driven_in_meters: 0]

  def new() do
    %RemoteControlCar{nickname: "none"}
  end

  def new(nickname) do
    %RemoteControlCar{nickname: nickname}
  end

  def display_distance(%RemoteControlCar{distance_driven_in_meters: distance_driven_in_meters}) do
    "#{distance_driven_in_meters} meters"
  end

  def display_battery(%RemoteControlCar{battery_percentage: battery_percentage}) do
    case battery_percentage do
      0 -> "Battery empty"
      _ -> "Battery at #{battery_percentage}%"
    end
  end

  def drive(%RemoteControlCar{battery_percentage: battery_percentage} = car) when battery_percentage == 0, do: car
  def drive(%RemoteControlCar{} = car) do
    car
    |> travel_meters(20)
    |> drain_battery(1)
  end

  defp travel_meters(%RemoteControlCar{distance_driven_in_meters: distance_driven_in_meters} = car, meters) do
    %{car | distance_driven_in_meters: distance_driven_in_meters + meters}
  end

  defp drain_battery(%RemoteControlCar{battery_percentage: battery_percentage} = car, percentage) do
    %{car | battery_percentage: battery_percentage - (percentage / 100 * battery_percentage)}
  end
end
