# Use the Plot struct as it is provided
defmodule Plot do
  @enforce_keys [:plot_id, :registered_to]
  defstruct [:plot_id, :registered_to]
end

defmodule CommunityGarden do
  def start(_opts \\ []) do
    Agent.start(fn -> %{plots: %{}, next_id: 1} end)
  end

  def list_registrations(pid) do
    Agent.get(pid, fn state -> Map.values(state.plots) end)
  end

  def register(pid, register_to) do
    Agent.get_and_update(pid, fn %{plots: plots, next_id: next_id} ->
      new_plot = %Plot{plot_id: next_id, registered_to: register_to}
      new_plots = Map.put(plots, next_id, new_plot)
      {new_plot, %{plots: new_plots, next_id: next_id + 1}}
    end)
  end

  def release(pid, plot_id) do
    Agent.update(pid, fn %{plots: plots} = state ->
      updated_plots = Map.delete(plots, plot_id)
      %{state | plots: updated_plots}
    end)
  end

  def get_registration(pid, plot_id) do
    Agent.get(pid, fn %{plots: plots} ->
      Map.get(plots, plot_id, {:not_found, "plot is unregistered"})
    end)
  end
end
