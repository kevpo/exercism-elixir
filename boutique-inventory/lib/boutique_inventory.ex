defmodule BoutiqueInventory do
  def sort_by_price(inventory) do
    Enum.sort(inventory, &(&1.price <= &2.price))
  end

  def with_missing_price(inventory) do
    Enum.filter(inventory, &(&1.price == nil))
  end

  def increase_quantity(%{quantity_by_size: quantity_by_size} = item, _count) when quantity_by_size == %{}, do: item
  def increase_quantity(%{quantity_by_size: quantity_by_size} = item, count) do
    new_quantities = Enum.map(quantity_by_size, fn {k, v} -> {k, v + count} end)
    |> Map.new()

    %{ item | quantity_by_size: new_quantities}
  end

  def total_quantity(%{quantity_by_size: quantity_by_size}) when quantity_by_size == %{}, do: 0
  def total_quantity(%{quantity_by_size: quantity_by_size}) do
    Enum.map(quantity_by_size, fn {_k, v} -> v end)
    |> Enum.sum()
  end
end
