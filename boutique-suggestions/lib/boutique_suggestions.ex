defmodule BoutiqueSuggestions do

  @default_max_price 100

  def get_combinations(tops, bottoms, options \\ []) do
    max_price = Keyword.get(options, :maximum_price, @default_max_price)

    for top <- tops,
        bottom <- bottoms,
        not clashing_colors(top, bottom),
        not price_too_high(top, bottom, max_price) do
      {top, bottom}
    end
  end

  defp clashing_colors(first_item, second_item) do
    first_item.base_color == second_item.base_color
  end

  defp price_too_high(first_item, second_item, max_price) do
    prices = Enum.map([first_item, second_item], fn i -> i.price end)
    Enum.sum(prices) > max_price
  end
end
