defmodule Year do
  @doc """
  Returns whether 'year' is a leap year.

  A leap year occurs:

  on every year that is evenly divisible by 4
    except every year that is evenly divisible by 100
      unless the year is also evenly divisible by 400
  """
  @spec leap_year?(non_neg_integer) :: boolean
  def leap_year?(year) do
    divisible_by_4 = rem(year, 4) == 0
    divisible_by_100 = rem(year, 100) == 0
    divisible_by_400 = rem(year, 400) == 0

    cond do
      divisible_by_4 and divisible_by_100 and divisible_by_400 -> true
      divisible_by_4 and divisible_by_100 and not divisible_by_400 -> false
      divisible_by_4 -> true
      true -> false
    end
  end
end
