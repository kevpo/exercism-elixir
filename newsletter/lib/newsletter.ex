defmodule Newsletter do
  def read_emails(path) do
    path
    |> File.read!()
    |> String.split("\n", trim: true)
  end

  def open_log(path) do
    File.open!(path, [:write])
  end

  def log_sent_email(pid, email) do
    IO.puts(pid, email)
  end

  def close_log(pid) do
    File.close(pid)
  end

  def send_newsletter(emails_path, log_path, send_fun) do
    emails = read_emails(emails_path)
    log_pid = open_log(log_path)
    send_emails(emails, send_fun, log_pid)
    close_log(log_pid)
  end

  defp send_emails(emails, send_fun, log_pid) do
    Enum.each(emails, fn email -> send_email(email, send_fun, log_pid) end)
  end

  defp send_email(email, send_fun, log_pid) do
    email
    |> send_fun.()
    |> log_email_sent(email, log_pid)
  end

  defp log_email_sent(:ok, email, log_pid) do
    log_sent_email(log_pid, email)
  end
  defp log_email_sent(_status, _email, _log_pid), do: nil
end
