defmodule DNA do
  def encode_nucleotide(code_point) do
    case code_point do
      code_point when code_point == ?\s -> 0b0000
      code_point when code_point == ?A -> 0b0001
      code_point when code_point == ?C -> 0b0010
      code_point when code_point == ?G -> 0b0100
      code_point when code_point == ?T -> 0b1000
      _ -> nil
    end
  end

  def decode_nucleotide(encoded_code) do
    case encoded_code do
      0b0000 -> ?\s
      0b0001 -> ?A
      0b0010 -> ?C
      0b0100 -> ?G
      0b1000 -> ?T
      _ -> nil
    end
  end

  def encode(dna) do
    do_encode(dna, <<>>)
  end

  defp do_encode([], encoded), do: encoded
  defp do_encode([head | tail], encoded) do
    encoded_nucleotide = <<encode_nucleotide(head)::4>>
    do_encode(tail, <<encoded::bitstring, encoded_nucleotide::bitstring>>)
  end

  def decode(dna) do
    do_decode(dna, [])
  end

  defp do_decode(<<>>, nucleotides), do: List.to_charlist(nucleotides)
  defp do_decode(<<value::4, rest::bitstring>>, nucleotides) do
    do_decode(rest, nucleotides ++ [decode_nucleotide(value)])
  end
end
