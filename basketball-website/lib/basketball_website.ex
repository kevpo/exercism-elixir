defmodule BasketballWebsite do
  def extract_from_path(data, path) do
    path
    |> String.split(".")
    |> do_extract(data)
  end

  defp do_extract([head | []], data), do: data[head]
  defp do_extract([head | tail], data), do: do_extract(tail, data[head])

  def get_in_path(data, path) do
    keys = String.split(path, ".")
    get_in(data, keys)
  end
end
